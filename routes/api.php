<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Test Users
Route::post('/UserCreate','UserController@greet');
Route::post('/umkm/kbli/kategoriCreate','CategoryController@CategoryCreate');
Route::get('/umkm/kbli/kategoris','CategoryController@CategoryAlls');
Route::get('/umkm/kbli/kategoriById','CategoryController@CategoryById');

/* KBLI - Golongan

*/
Route::get('/umkm/kbli/golongans','KbliGolonganController@Golongans');
Route::get('/umkm/kbli/GolonganById','KbliGolonganController@GolonganById');
Route::post('/umkm/kbli/GolonganCreate','KbliGolonganController@GolonganCreate');

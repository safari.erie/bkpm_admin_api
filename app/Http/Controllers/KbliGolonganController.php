<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class KbliGolonganController extends Controller
{
    //
    /**
     * @OA\Get(
     *     path="/api/umkm/kbli/golongans",
     *     tags={"KBLI_GolonganAll"},
     *     summary="Return All Data Golongan",
     *     description="Retrive data golongan",
     *     operationId="greet",

     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function Golongans(){

        $golongans = array(
            [
                'kd_kategori' => 'A',
                'kd_gol'   => '1',
                'kd_kbli'   => '01',
                'gol_nm'    => 'Pertanian Tanaman, Peternakan, Perburuan dan Kegiatan YBDI',
                'gol_des'   => 'Golongan pokok ini mencakup pertanian tanaman pangan, perkebunan dan hortikultura; usaha pemeliharaan hewan ternak dan unggas; perburuan dan penangkapan hewan dengan perangkap serta kegiatan penunjang ybdi yang ditujukan untuk dijual. Termasuk budidaya tanaman dan hewan ternak secara organik dan genetik. Kegiatan pertanian tidak mencakup kegiatan pengolahan dari komoditas pertanian,  termasuk dalam Kategori C (Industri Pengolahan). Kegiatan konstruksi lahan seperti pembuatan petak-petak sawah, irigasi saluran pembuangan air, serta pembersihan dan perbaikan lahan untuk pertanian tidak termasuk di sini, tetapi tercakup pada kategori konstruksi (F).',

            ],
            [
                'kd_kategori' => 'A',
                'kd_gol'   => '2',
                'kd_kbli'   => '02',
                'gol_nm'    => '',
                'gol_des'   => '',

            ],
            [
                'kd_kategori' => 'A',
                'kd_gol'   => '2',
                'kd_kbli'   => '02',
                'gol_nm'    => '',
                'gol_des'   => '',

            ],
            [
                'kd_kategori' => 'A',
                'kd_gol'   => '2',
                'kd_kbli'   => '02',
                'gol_nm'    => 'Pengelolaan Kehutanan dan Penebangan',
                'gol_des'   => 'Golongan pokok ini mencakup produksi kayu bulat untuk industri manufaktur berbasis hutan (Golongan Pokok 16 dan 17) serta ekstraksi dan pengumpulan/pemungutan produk hutan non-kayu yang tumbuh liar. Selain produksi kayu, kegiatan kehutanan menghasilkan produk yang hanya diproses sedikit, seperti kayu bakar, arang, serpihan kayu dan kayu bulat yang digunakan dalam bentuk yang tidak diproses (mis. Pit-props, pulpwood, dll.). Kegiatan ini dapat dilakukan di hutan alam atau hutan tanaman.',

            ],
            [
                'kd_kategori' => 'A',
                'kd_gol'   => '3',
                'kd_kbli'   => '03',
                'gol_nm'    => 'Perikanan',
                'gol_des'   => 'Golongan pokok ini mencakup penangkapan dan budidaya ikan, jenis crustacea (seperti udang, kepiting) mollusca, dan biota air lainnya di laut, air payau dan air tawar. Tidak termasuk pemancingan untuk rekreasi.',

            ],
            [
                'kd_kategori' => 'B',
                'kd_gol'   => '5',
                'kd_kbli'   => '05',
                'gol_nm'    => 'Pertambangan Batu Bara dan Lignit',
                'gol_des'   => 'Golongan pokok ini mencakup pertambangan batu bara dan lignit melalui pertambangan bawah tanah atau pertambangan terbuka. Kegiatan ini juga mencakup pekerjaan seperti penggolongan, pembersihan, pemadatan dan langkah-langkah lain yang diperlukan dalam pengangkutan untuk dijual. Proses lainnya seperti pembuatan kokas (191) dari mineral dan jasa pertambangan batu bara dan lignit (099) atau pembuatan briket (192) tidak dicakup dalam golongan pokok ini.',

            ],
            [
                'kd_kategori' => 'B',
                'kd_gol'   => '6',
                'kd_kbli'   => '06',
                'gol_nm'    => 'Pertambangan Minyak Bumi dan Gas Alam Dan Panas Bumi',
                'gol_des'   => 'Golongan pokok ini mencakup produksi minyak bumi mentah, pertambangan dan pengambilan minyak dari serpihan minyak dan pasir minyak dan produksi gas alam serta pencarian cairan hidrokarbon. Golongan pokok ini juga mencakup kegiatan operasi dan atau pengembangan lokasi penambangan minyak dan gas.',

            ],
            [
                'kd_kategori' => 'B',
                'kd_gol'   => '7',
                'kd_kbli'   => '07',
                'gol_nm'    => 'Pertambangan Bijih Logam',
                'gol_des'   => 'Golongan pokok ini mencakup pengambilan mineral dari tambang dan galian, juga pengerukan tanah endapan, penghancuran batu dan pengambilan garam. Sebagian besar hasil pertambangan dan penggalian mineral ini digunakan pada bidang konstruksi (pasir, batu dan lain-lain), industri bahan galian (tanah liat, gips, kapur dan lain-lain), industri bahan-bahan kimia dan lain-lain. Golongan pokok ini juga mencakup kegiatan penghancuran, pengasahan, pemotongan, pembersihan, pengeringan, sortasi dan pencampuran bahan-bahan mineral tersebut.',

            ]
            /* ,
            [
                'kd_kategori' => 'B',
                'kd_gol'   => '8',
                'kd_kbli'   => '08',
                'gol_nm'    => 'Pertambangan dan Penggalian Lainnya',
                'gol_des'   => 'Golongan pokok ini mencakup pengambilan mineral dari tambang dan galian, juga pengerukan tanah endapan, penghancuran batu dan pengambilan garam. Sebagian besar hasil pertambangan dan penggalian mineral ini digunakan pada bidang konstruksi (pasir, batu dan lain-lain), industri bahan galian (tanah liat, gips, kapur dan lain-lain), industri bahan-bahan kimia dan lain-lain. Golongan pokok ini juga mencakup kegiatan penghancuran, pengasahan, pemotongan, pembersihan, pengeringan, sortasi dan pencampuran bahan-bahan mineral tersebut.',
            ],
            [
                'kd_kategori' => 'A',
                'kd_gol'   => '2',
                'kd_kbli'   => '02',
                'gol_nm'    => '',
                'gol_des'   => '',

            ], */
        );

        $responses = array(
            'status' => 'OK',
            'datas' => $golongans,
            'returnMessage' => ''
        );

        return response()->json($responses,HttpFoundationResponse::HTTP_OK);

    }

     /**
     * @OA\Get(
     *     path="/api/umkm/kbli/GolonganById",
     *     tags={"KBLI_GolonganById"},
     *     summary="Return Spesific data golongan",
     *     description="Retrive data golongan by id_golongan",
     *     operationId="golongan",
     *     @OA\Parameter(
     *          name="id",
     *          description="id",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */

     /**
      * Get resource from database by id
      */

    public function GolonganById (Request $request){
          $param = $request->only([
              'id'
            ]);

           $golongan = array(
                'kd_kategori' => 'A',
                'kd_gol'   => '1',
                'kd_kbli'   => '01',
                'gol_nm'    => 'Pertanian Tanaman, Peternakan, Perburuan dan Kegiatan YBDI',
                'gol_des'   => 'Golongan pokok ini mencakup pertanian tanaman pangan, perkebunan dan hortikultura; usaha pemeliharaan hewan ternak dan unggas; perburuan dan penangkapan hewan dengan perangkap serta kegiatan penunjang ybdi yang ditujukan untuk dijual. Termasuk budidaya tanaman dan hewan ternak secara organik dan genetik. Kegiatan pertanian tidak mencakup kegiatan pengolahan dari komoditas pertanian,  termasuk dalam Kategori C (Industri Pengolahan). Kegiatan konstruksi lahan seperti pembuatan petak-petak sawah, irigasi saluran pembuangan air, serta pembersihan dan perbaikan lahan untuk pertanian tidak termasuk di sini, tetapi tercakup pada kategori konstruksi (F).',
           );

            $responses = array(
            'status' => 'OK',
            'datas' => $golongan,
            'returnMessage' => ''
        );

        return response()->json($responses,HttpFoundationResponse::HTTP_OK);


    }


}

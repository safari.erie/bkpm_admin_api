<?php

namespace App\Http\Controllers;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Repository\EloquentCategoryRepository;
use App\Repository\CategoryRepository;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class CategoryController extends Controller
{
    protected $eloquentCategory;
    //
    public function __construct(EloquentCategoryRepository $eloquentCategory){
        $this->eloquentCategory = $eloquentCategory;
    }

    /**
     * @OA\Get(
     *     path="/api/umkm/kbli/kategoris",
     *     tags={"KBLI_KategoriAll"},
     *     summary="Return All Category Data",
     *     description="Retrive data category all",
     *     operationId="greet",

     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */

     public function CategoryAlls(){
        $categorys = $this->eloquentCategory->findAllCategorys();
        $responses = array(
            'status' => 'OK',
            'datas'  => $categorys,
            'returnMessage' => ''
        );
         return response()->json($responses, HttpFoundationResponse::HTTP_OK);

     }

     /**
     * @OA\Get(
     *     path="/api/umkm/kbli/kategoriById",
     *     tags={"KBLI_kategoriId"},
     *     summary="Return Spesific data",
     *     description="Retrive data category by kategori_id",
     *     operationId="categori",
     *     @OA\Parameter(
     *          name="id",
     *          description="id",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */

     /**
      * Get resource from database by id
      */

      public function CategoryById (Request $request){
          $param = $request->only([
              'id'
            ]);
            $category = $this->eloquentCategory->findCategoryById($param['id']);
            if($category['message'] == ''){
                $responses = array(
                    'status' => 'OK',
                    'data'  => $category['data'],
                    'returnMessage' => ''
                );
                return response()->json($responses, HttpFoundationResponse::HTTP_OK);
            }else{
                $responses = array(
                    'status' => 'Not',
                    'data'  => $category['data'],
                    'returnMessage' => $category['message']
                );
                return response()->json($responses, HttpFoundationResponse::HTTP_NOT_FOUND);
            }
        }


        /**
        * @OA\POST(
        *     path="/api/umkm/kbli/kategoriCreate",
        *     tags={"KBLI_KategoriCreate"},
        *     summary="Save Category",
        *     description="Save Category into database",
        *     operationId="category Save",
        *     @OA\Parameter(
        *          name="kd_kategori",
        *          description="kode kategori",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *     ),
        *     @OA\Parameter(
        *          name="kategori_nm",
        *          description="Nama kategori",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *     ),
         *     @OA\Parameter(
        *          name="kategori_desc",
        *          description="Deskripsi kategori",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *     ),
        *     @OA\Response(
        *         response="default",
        *         description="successful operation"
        *     )
        * )
        */

        /**
         * Store data to database into table tb_kategori
         * @param : object categoryData
         */
        public function CategoryCreate(Request $request){
            $validator = Validator::make($request->all(),[
                'kd_kategori' => ['required'],
                'kategori_nm' => ['required'],
                'kategori_desc' => ['required'],
            ]);

            if($validator->fails()){
                return response()->json([
                    'status' => HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'data'  => null,
                    'returnMessage' => $validator->errors()
                ],HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $saveCategory = $this->eloquentCategory->saveCategory($request);
            $responses = array(
                'status' => $saveCategory['message'] == null ? 'OK' : 'Not Ok',
                'data'  => null,
                'responseMessage' => $saveCategory['message'] == null ? 'Data Berhasil Di Simpan' : $saveCategory['message']
            );

            return response()->json($responses, $saveCategory['message'] == null ? HttpFoundationResponse::HTTP_OK : HttpFoundationResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

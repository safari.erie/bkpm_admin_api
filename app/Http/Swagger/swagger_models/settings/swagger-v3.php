<?php

/**
 * @OA\Info(
 *     description="API doc",
 *     version="1.0.0",
 *     title="PIR API documentation ",
 *     termsOfService="http://swagger.io/terms/",
 *     @OA\Contact(
 *         email="safari.erie@gmail.com"
 *     ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 */


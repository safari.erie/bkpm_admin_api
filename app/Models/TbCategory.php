<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TbCategory extends Model
{
    //
    protected $table = 'umkm.tb_kategori';
    public $timestamps = false;
    protected $primaryKey = 'id_kategori';
}

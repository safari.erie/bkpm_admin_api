<?php

namespace App\Repository;

    /**
     * createdby : eri.safari
     * createdt : 10 June 2021
     * interface repository
     *
     */

    interface CategoryRepository{
        public function findAllCategorys();
        public function findCategoryById($IdKategory);
        public function saveCategory($dataCategory);
    }

?>

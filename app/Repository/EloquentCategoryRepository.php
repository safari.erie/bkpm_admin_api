<?php

namespace App\Repository;
use App\Models\TbCategory;
use Illuminate\Database\QueryException;

class EloquentCategoryRepository implements CategoryRepository{
    /**
     * createdby : eri.safari
     * createdt :
     * Implement interface kategori
     *
     */

    public function findAllCategorys(){
        $categorys = TbCategory::all();

        return $categorys;
    }

    public function findCategoryById($IdKategory){

        $message = "";
        if(TbCategory::where('id_kategori',$IdKategory)->exists()){
            $category = TbCategory::where('id_kategori',$IdKategory)->first();
            $response = array(
                'message' => '',
                'data' => $category
            );
            return $response;

        }else{
            $response = array(
                'message' => 'Kategori not found',
                'data' => null
            );

            return $response;
        }

    }

    public function saveCategory($dataCategory){

        try {
            //code...
            $objCategory = new TbCategory;
            $objCategory->kd_kategori = $dataCategory->kd_kategori;
            $objCategory->kategori_nm = $dataCategory->kategori_nm;
            $objCategory->kategori_desc = $dataCategory->kategori_desc;
            $objCategory->save();
            $response = [
                'message' => null,

            ];
            return $response;

        } catch (QueryException $ex) {
            //throw $th;
            $response = [
                'message' => "Failed ". $ex->errorInfo,

            ];
            return $response;
        }
    }
}
?>

